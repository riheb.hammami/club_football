<?php

namespace App\DataFixtures;

use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(PersistenceObjectManager $manager)
    {
        $utilisateur = new Utilisateur();
        $utilisateur->setEmail("admin@gmail.com");
        $utilisateur->setRoles(["ROLE_ADMIN"]);
        $password = $this->encoder->encodePassword($utilisateur, 'admin123');
        $utilisateur->setPassword($password);

        $manager->persist($utilisateur);
        $manager->flush();
    }
}

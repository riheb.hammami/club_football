<?php

namespace App\DataFixtures;

use App\Entity\Club;
use App\Entity\Joueur;
use App\Entity\Logo;
use App\Entity\Saison;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // for($i=1 ; $i<=5 ; $i++){    
        //   $club = new Club();
        //   $club->setNom("club".$i);
        //   $manager->persist($club);
        //   $logo = new Logo();
        //   $logo->setClub($club);s
        //   $logo->setImage("club".$i.".png");
        //   $logo->setDateDebut(new \DateTime('2020-11-14'));
        //   $logo->setDateFin(new \DateTime('2021-11-14'));
        //   $manager->persist($logo);
        // }

        $club1 = new Club();
        $club1->setNom("club1");
        $manager->persist($club1);
        $logo1 = new Logo();
        $logo1->setClub($club1);
        $logo1->setImage("club1.png");
        $logo1->setDateDebut(new \DateTime('2020-11-14'));
        $logo1->setDateFin(new \DateTime('2021-11-14'));
        $manager->persist($logo1);

        $club2 = new Club();
        $club2->setNom("club2");
        $manager->persist($club2);
        $logo2 = new Logo();
        $logo2->setClub($club2);
        $logo2->setImage("club2.png");
        $logo2->setDateDebut(new \DateTime('2018-01-01'));
        $logo2->setDateFin(new \DateTime('2020-01-01'));
        $manager->persist($logo2);

        $club3 = new Club();
        $club3->setNom("club3");
        $manager->persist($club3);
        $logo3 = new Logo();
        $logo3->setClub($club3);
        $logo3->setImage("club3.png");
        $logo3->setDateDebut(new \DateTime('2016-01-01'));
        $logo3->setDateFin(new \DateTime('2021-01-01'));
        $manager->persist($logo3);


        $joueur1 = new Joueur();
        $joueur1->setPrenom('Tim');
        $joueur1->setNom('Aelbrecht');
        $manager->persist($joueur1);

        $joueur2 = new Joueur();
        $joueur2->setPrenom('Daniel');
        $joueur2->setNom('Alberto');
        $manager->persist($joueur2);

        $joueur3 = new Joueur();
        $joueur3->setPrenom('Zakaria');
        $joueur3->setNom('Alaoui');
        $manager->persist($joueur3);

        $joueur4 = new Joueur();
        $joueur4->setPrenom('Alim');
        $joueur4->setNom('Ben Mabrouk');
        $manager->persist($joueur4);

        $joueur5 = new Joueur();
        $joueur5->setPrenom('Michel');
        $joueur5->setNom('Bensoussan');
        $manager->persist($joueur5);

        $joueur6 = new Joueur();
        $joueur6->setPrenom('Youcef');
        $joueur6->setNom('Bouarfa');
        $manager->persist($joueur6);

        $saison1 = new Saison();
        $saison1->setJoueur($joueur1);
        $saison1->setClub($club1);
        $saison1->setNumeroSpecifique(213);
        $saison1->setNbreButs(3);
        $saison1->setAnneeDebut(2019);
        $saison1->setAnneeFin(2020);
        $manager->persist($saison1);

        $saison2 = new Saison();
        $saison2->setJoueur($joueur1);
        $saison2->setClub($club2);
        $saison2->setNumeroSpecifique(587);
        $saison2->setNbreButs(2);
        $saison2->setAnneeDebut(2017);
        $saison2->setAnneeFin(2018);
        $manager->persist($saison2);

        $saison3 = new Saison();
        $saison3->setJoueur($joueur2);
        $saison3->setClub($club2);
        $saison3->setNumeroSpecifique(777);
        $saison3->setNbreButs(1);
        $saison3->setAnneeDebut(2017);
        $saison3->setAnneeFin(2018);
        $manager->persist($saison3);

        $saison4 = new Saison();
        $saison4->setJoueur($joueur2);
        $saison4->setClub($club3);
        $saison4->setNumeroSpecifique(369);
        $saison4->setNbreButs(2);
        $saison4->setAnneeDebut(2017);
        $saison4->setAnneeFin(2018);
        $manager->persist($saison4);

        $manager->flush();
    }
}

<?php

namespace App\Controller;

use App\Entity\Club;
use App\Repository\SaisonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class ClubController extends AbstractController
{



    public function __construct(SaisonRepository $saisonRepository)
    {
        $this->saisonRepository = $saisonRepository;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $clubs = $this->getDoctrine()->getRepository(Club::class)->findAll();
        $saisons = $this->saisonRepository->getDistinctSaisons();
        return $this->render('club/index.html.twig', [
            'clubs' => $clubs,
            'saisons' => $saisons,
            //'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/club/{id}", name="club_show")
     */
    public function showPlayersOfClub($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $session = new Session();
        $annee_debut = $session->get('annee_debut')?? 0;
        $annee_fin = $session->get('annee_fin')?? 0;
        
        $joueurs = $this->saisonRepository
            ->findJoueurOfSaisonClub($annee_debut, $annee_fin, $id);
        $saisons = $this->saisonRepository->getDistinctSaisons();

        return $this->render('club/joueurs_of_club.html.twig', [
            'joueurs' => $joueurs,
            'club_id' => $id,
            'saisons' => $saisons
        ]);
    }

    /**
     * @Route("/joueur/{id}", name="show_clubs_player")
     */
    public function showClubsOfPlayer($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $session = new Session();
        $annee_debut = $session->get('annee_debut');
        $annee_fin = $session->get('annee_fin');

        $clubs = $this->saisonRepository
            ->findclubsOfSaisonJoueur($annee_debut, $annee_fin, $id);
        $saisons = $this->saisonRepository->getDistinctSaisons();

        return $this->render('club/clubs_of_joueur.html.twig', [
            'clubs' => $clubs,
            'joueur_id' => $id,
            'saisons' => $saisons
        ]);
    }


    /**
     * Returns a JSON string with the clubs  of the Saison.
     * 
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/saison_clubs", name="saison_list_clubs")
     */
    public function getClubsOfSaison()
    {

        $request = Request::createFromGlobals();
        $annee_debut = $request->get('annee_debut');
        $annee_fin = $request->get('annee_fin');

        $session = new Session();
        $session->set('annee_debut', $annee_debut);
        $session->set('annee_fin', $annee_fin);

        $clubs = $this->saisonRepository
            ->findClubsOfSaison($annee_debut, $annee_fin);
        return new JsonResponse($clubs);
    }
    /**
     * Returns a JSON string with the players of the Saison & club.
     * 
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/saison_joueurs", name="saison_list_joueurs_club")
     */
    public function getPlayersOfSaison()
    {
        
        $request = Request::createFromGlobals();
        $annee_debut = $request->get('annee_debut');
        $annee_fin = $request->get('annee_fin');
        $club_id = $request->get('club_id');

        $session = new Session();
        $session->set('annee_debut', $annee_debut);
        $session->set('annee_fin', $annee_fin);

        $joueurs = $this->saisonRepository->findJoueurOfSaisonClub($annee_debut, $annee_fin, $club_id);
        return new JsonResponse($joueurs);
    }


    /**
     * Returns a JSON string with the players of the Saison & club.
     * 
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/saison_clubs_joueur", name="saison_list_clubs_joueur")
     */
    public function getClubsOfSaisonPlayer()
    {
       
        $request = Request::createFromGlobals();
        $annee_debut = $request->get('annee_debut');
        $annee_fin = $request->get('annee_fin');
        $joueur_id = $request->get('joueur_id');

        $session = new Session();
        $session->set('annee_debut', $annee_debut);
        $session->set('annee_fin', $annee_fin);

        $clubs = $this->saisonRepository
            ->findclubsOfSaisonJoueur($annee_debut, $annee_fin, $joueur_id);

        return new JsonResponse($clubs);
    }
}

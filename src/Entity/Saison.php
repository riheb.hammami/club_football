<?php

namespace App\Entity;

use App\Repository\SaisonRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SaisonRepository::class)
 */
class Saison
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $annee_debut;

    /**
     * @ORM\Column(type="integer")
     */
    private $annee_fin;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbre_buts;

    /**
     * @ORM\Column(type="integer")
     */
    private $numero_specifique;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Joueur", cascade={"persist"})
     */
    private $joueur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Club", cascade={"persist"})
     */
     private $club;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnneeDebut(): ?int
    {
        return $this->annee_debut;
    }

    public function setAnneeDebut(int $annee_debut): self
    {
        $this->annee_debut = $annee_debut;

        return $this;
    }

    public function getAnneeFin(): ?int
    {
        return $this->annee_fin;
    }

    public function setAnneeFin(int $annee_fin): self
    {
        $this->annee_fin = $annee_fin;

        return $this;
    }

    public function getNbreButs(): ?int
    {
        return $this->nbre_buts;
    }

    public function setNbreButs(?int $nbre_buts): self
    {
        $this->nbre_buts = $nbre_buts;

        return $this;
    }

    public function getNumeroSpecifique(): ?int
    {
        return $this->numero_specifique;
    }

    public function setNumeroSpecifique(int $numero_specifique): self
    {
        $this->numero_specifique = $numero_specifique;

        return $this;
    }

    public function getJoueur(): ?Joueur
    {
        return $this->joueur;
    }

    public function setJoueur(?Joueur $joueur): self
    {
        $this->joueur = $joueur;

        return $this;
    }

    public function getClub(): ?Club
    {
        return $this->club;
    }

    public function setClub(?Club $club): self
    {
        $this->club = $club;

        return $this;
    }
}

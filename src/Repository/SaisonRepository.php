<?php

namespace App\Repository;

use App\Entity\Saison;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Saison|null find($id, $lockMode = null, $lockVersion = null)
 * @method Saison|null findOneBy(array $criteria, array $orderBy = null)
 * @method Saison[]    findAll()
 * @method Saison[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaisonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Saison::class);
    }

    /**  Get Distinct saisons */
    public function getDistinctSaisons()
    {
        return $this->createQueryBuilder('s')
            ->select('s.annee_debut', 's.annee_fin')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**  Get clubs of saison */
    public function findClubsOfSaison($annee_debut, $annee_fin)
    {
        $sql = "SELECT distinct c.id, c.nom FROM club c, saison s  where c.id=s.club_id and annee_debut=$annee_debut and annee_fin=$annee_fin;";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        //$stmt->execute(array('annee_debut' => $annee_debut,'annee_fin' => $annee_fin));
        return $stmt->fetchAll();
    }
    /**  Get players of current saison & club */
    public function findJoueurOfSaisonClub($annee_debut, $annee_fin, $club_id)
    {
        $sql = "SELECT j.* FROM joueur j, saison s  where j.id=s.joueur_id and s.club_id=$club_id and annee_debut=$annee_debut and annee_fin=$annee_fin;";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        //$stmt->execute(array('annee_debut' => $annee_debut,'annee_fin' => $annee_fin));
        return $stmt->fetchAll();
    }

    /**  Get clubs of current player & club */
    public function findclubsOfSaisonJoueur($annee_debut, $annee_fin, $joueur_id)
    {
        $sql = "SELECT c.*, s.* FROM club c, saison s  where c.id=s.club_id and s.joueur_id=$joueur_id and annee_debut=$annee_debut and annee_fin=$annee_fin;";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        //$stmt->execute(array('annee_debut' => $annee_debut,'annee_fin' => $annee_fin));
        return $stmt->fetchAll();
    }

    // /**
    //  * @return Saison[] Returns an array of Saison objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Saison
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
